package com.example.aves.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tont_paises")
@EntityListeners(AuditingEntityListener.class)
public class Tont_Pais implements Serializable {

    @Id
    @Size(max = 3)
    private String cdpais;

    @NotBlank
    @Size(max = 100)
    private String dsnombre;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cdzona", nullable = false)
    private Tont_Zona cdzona;

    public String getCdpais() {
        return cdpais;
    }

    public void setCdpais(String cdpais) {
        this.cdpais = cdpais;
    }

    public String getDsnombre() {
        return dsnombre;
    }

    public void setDsnombre(String dsnombre) {
        this.dsnombre = dsnombre;
    }

    public Tont_Zona getCdzona() {
        return cdzona;
    }

    public void setCdzona(Tont_Zona cdzona) {
        this.cdzona = cdzona;
    }

}

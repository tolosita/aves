package com.example.aves.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tont_zonas")
@EntityListeners(AuditingEntityListener.class)
public class Tont_Zona implements Serializable {

    @Id
    @Size(max = 3)
    private String cdzona;

    @NotBlank
    @Size(max = 45)
    private String dsnombre;

    public String getCdzona() {
        return cdzona;
    }

    public void setCdzona(String cdzona) {
        this.cdzona = cdzona;
    }

    public String getDsnombre() {
        return dsnombre;
    }

    public void setDsnombre(String dsnombre) {
        this.dsnombre = dsnombre;
    }

}

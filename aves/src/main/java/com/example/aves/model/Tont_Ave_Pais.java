package com.example.aves.model;

import java.io.Serializable;
import javax.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tont_aves_pais")
@EntityListeners(AuditingEntityListener.class)
public class Tont_Ave_Pais implements Serializable {

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cdpais", nullable = false)
    private Tont_Pais cdpais;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cdave", nullable = false)
    private Tont_Ave cdave;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Tont_Pais getCdpais() {
        return cdpais;
    }

    public void setCdpais(Tont_Pais cdpais) {
        this.cdpais = cdpais;
    }

    public Tont_Ave getCdave() {
        return cdave;
    }

    public void setCdave(Tont_Ave cdave) {
        this.cdave = cdave;
    }

}

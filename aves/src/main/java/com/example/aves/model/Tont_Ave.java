package com.example.aves.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "tont_aves")
@EntityListeners(AuditingEntityListener.class)
public class Tont_Ave implements Serializable {

    @Id
    @Size(max = 5)
    private String cdave;

    @NotBlank
    @Size(max = 100)
    private String dsnombre_comun;

    @NotBlank
    @Size(max = 200)
    private String dsnombre_cientifico;

    public String getCdave() {
        return cdave;
    }

    public void setCdave(String cdave) {
        this.cdave = cdave;
    }

    public String getDsnombre_comun() {
        return dsnombre_comun;
    }

    public void setDsnombre_comun(String dsnombre_comun) {
        this.dsnombre_comun = dsnombre_comun;
    }

    public String getDsnombre_cientifico() {
        return dsnombre_cientifico;
    }

    public void setDsnombre_cientifico(String dsnombre_cientifico) {
        this.dsnombre_cientifico = dsnombre_cientifico;
    }

}

package com.example.aves.repository;

import com.example.aves.model.Tont_Zona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZonaRepository extends JpaRepository<Tont_Zona, String> {

}

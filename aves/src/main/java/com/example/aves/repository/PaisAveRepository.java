package com.example.aves.repository;

import com.example.aves.model.Tont_Ave_Pais;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisAveRepository extends JpaRepository<Tont_Ave_Pais, String> {

}

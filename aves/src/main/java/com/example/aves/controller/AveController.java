package com.example.aves.controller;

import com.example.aves.model.Tont_Ave;
import com.example.aves.repository.AveRepository;
import com.example.aves.repository.PaisAveRepository;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AveController {

    @Autowired
    AveRepository aveRepository;

    @Autowired
    PaisAveRepository paisAveRepository;

    // Get All Aves
    @GetMapping("/aves")
    public List<Tont_Ave> getAllAves(@RequestParam(value = "ave") String ave, @RequestParam(value = "zona") String zona) {
        List<Tont_Ave> aves = aveRepository.findAll();
        if (!ave.equals("")) {
            aves = aves.stream()
                    .filter(a -> a.getDsnombre_comun().contains(ave) || a.getDsnombre_cientifico().contains(ave))
                    .collect(Collectors.toList());
        }
        if (!zona.equals("")) {
            aves = paisAveRepository.findAll().stream()
                    .filter(paisAves -> paisAves.getCdpais().getCdzona().getCdzona().equals(zona) && (paisAves.getCdave().getDsnombre_comun().contains(ave) || paisAves.getCdave().getDsnombre_cientifico().contains(ave)))
                    .map(a -> a.getCdave())
                    .collect(Collectors.toList());
        }
        return aves;
    }

    // Create a new Aves
    @PostMapping("/aves")
    public Tont_Ave createAve(@Valid @RequestBody Tont_Ave ave) {
        return aveRepository.save(ave);
    }

    // Update a Aves
    @PutMapping("/aves/{id}")
    public Tont_Ave updateAve(@PathVariable(value = "id") String cdave, @Valid @RequestBody Tont_Ave aveDetails) {
        Tont_Ave ave = aveRepository.findById(cdave).orElse(null);

        ave.setDsnombre_comun(aveDetails.getDsnombre_comun());
        ave.setDsnombre_cientifico(aveDetails.getDsnombre_cientifico());

        return aveRepository.save(ave);
    }

    // Delete a Aves
    @DeleteMapping("/aves/{id}")
    public ResponseEntity<?> deleteAve(@PathVariable(value = "id") String cdave) {
        Tont_Ave ave = aveRepository.findById(cdave).orElse(null);

        aveRepository.delete(ave);

        return ResponseEntity.ok().build();
    }

}

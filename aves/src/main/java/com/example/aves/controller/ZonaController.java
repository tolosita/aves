package com.example.aves.controller;

import com.example.aves.model.Tont_Zona;
import com.example.aves.repository.ZonaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class ZonaController {

    @Autowired
    ZonaRepository zonaRepository;

    // Get All Zonas
    @GetMapping("/zonas")
    public List<Tont_Zona> getAllAves() {
        return zonaRepository.findAll();
    }

}

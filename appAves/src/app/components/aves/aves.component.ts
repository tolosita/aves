import { Component, OnInit } from '@angular/core';
import { Ave } from '../../models/ave';
import { Zona } from '../../models/zona';
import { AveService } from '../../services/ave.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-aves',
  templateUrl: './aves.component.html',
  styles: []
})
export class AvesComponent implements OnInit {
  aves: Ave[] = [];
  zonas: Zona[] = [];
  nombreB: string = '';
  zonaB: string = '';
  aveForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private aveService: AveService
  ) {
    this.aveForm = this.formBuilder.group({
      'cdave': [null, Validators.required],
      'dsnombre_comun': [null, Validators.required],
      'dsnombre_cientifico': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.getAves();
    this.getZonas();
    $('.progress').show();
  }

  getAves() {
    this.aveService.cargarAves(this.nombreB, this.zonaB).then((listado: Ave[]) => {
      this.aves = listado;
      $('.progress').hide();
    });
  }

  getZonas() {
    this.aveService.cargarZonas().then((listado: Zona[]) => this.zonas = listado);
  }

  guardar() {
    if (this.aveForm.status === "VALID") {
      $('.progress').show();
      this.aveService.guardarAve(this.aveForm.value).then(_ => {
        this.nuevo();
        this.getAves();
        $('#aveModal').modal('hide');
      });
    }
  }

  eliminar(id) {
    $('.progress').show();
    this.aveService.eliminarAve(id).then(_ => this.getAves()).catch(_ => {
      alert("No se pudo eliminar por que tiene relacionado un pais");
      $('.progress').hide();
    });
  }

  buscar(ave: Ave) {
    this.aveForm.controls['cdave'].setValue(ave.cdave);
    this.aveForm.controls['dsnombre_comun'].setValue(ave.dsnombre_comun);
    this.aveForm.controls['dsnombre_cientifico'].setValue(ave.dsnombre_cientifico);
    $('#codigo').prop('disabled', true);
  }

  nuevo() {
    this.aveForm.reset();
    $('#codigo').prop('disabled', false);
  }

  get codigo() { return this.aveForm.get('cdave'); }

  get nombreComun() { return this.aveForm.get('dsnombre_comun'); }

  get nombreCientifico() { return this.aveForm.get('dsnombre_cientifico'); }

}

import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ave } from '../models/ave';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AveService {
  path: string = 'http://localhost:8090/appAves/api';

  constructor(private http: HttpClient) { }

  cargarAves(nombre: string, zona: string) {
    let promise = new Promise((resolve, reject) => {
      this.http.get(`${this.path}/aves?ave=${nombre}&zona=${zona}`)
        .toPromise()
        .then(
          resp => {
            resolve(resp);
          },
          error => {
            reject(error);
          },
      );
    });
    return promise;
  }

  cargarZonas() {
    let promise = new Promise((resolve, reject) => {
      this.http.get(`${this.path}/zonas`)
        .toPromise()
        .then(
          resp => {
            resolve(resp);
          },
          error => {
            reject(error);
          },
      );
    });
    return promise;
  }

  guardarAve(data: Ave) {
    let promise = new Promise((resolve, reject) => {
      this.http.post(`${this.path}/aves`, data, httpOptions)
        .toPromise()
        .then(
          resp => {
            resolve(resp);
          },
          error => {
            reject(error);
          },
      );
    });
    return promise;
  }

  eliminarAve(data: any) {
    let promise = new Promise((resolve, reject) => {
      this.http.delete(`${this.path}/aves/${data}`)
        .toPromise()
        .then(
          resp => {
            resolve(resp);
          },
          error => {
            reject(error);
          },
      );
    });
    return promise;
  }

}

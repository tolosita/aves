import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AvesComponent } from './components/aves/aves.component';

import { AveService } from './services/ave.service';

@NgModule({
  declarations: [
    AppComponent,
    AvesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AveService],
  bootstrap: [AppComponent]
})
export class AppModule { }
